// Content.js
import React from 'react';
import './Content.css';

function Content({ selectedTab }) {
  return (
    <div className="content">
      {renderContent(selectedTab)}
    </div>
  );
}

function renderContent(selectedTab) {
  switch (selectedTab) {
    case 'Ana Sayfa':
      return <h1>Ana Sayfa İçeriği</h1>;
    case 'Ürünler':
      return <h1>Ürünler İçeriği</h1>;
    case 'Kategoriler':
      return <h1>Kategoriler İçeriği</h1>;
    case 'Hesaplar':
      return <h1>Hesaplar İçeriği</h1>;
    case 'Siparişler':
      return <h1>Siparişler İçeriği</h1>;
    default:
      return <h1>Ana Sayfa İçeriği</h1>;
  }
}

export default Content;
