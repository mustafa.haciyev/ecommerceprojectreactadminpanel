// Sidebar.js
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Sidebar.css'; // Sidebar bileşeni için CSS dosyası

function Sidebar() {
  const [selectedTab, setSelectedTab] = useState('Ana Sayfa');

  const changeTab = (tabName) => {
    setSelectedTab(tabName);
  };

  return (
    <div className="sidebar">
      <ul className="menu-list">
        <li className={selectedTab === 'Ana Sayfa' ? 'active' : ''} onClick={() => changeTab('Ana Sayfa')}>
          <Link to="/">Ana Sayfa</Link>
        </li>
        <li className={selectedTab === 'Ürünler' ? 'active' : ''} onClick={() => changeTab('Ürünler')}>
          <Link to="/products">Ürünler</Link>
        </li>
        <li className={selectedTab === 'Kategoriler' ? 'active' : ''} onClick={() => changeTab('Kategoriler')}>
          <Link to="/categories">Kategoriler</Link>
        </li>
        <li className={selectedTab === 'Hesaplar' ? 'active' : ''} onClick={() => changeTab('Hesaplar')}>
          <Link to="/accounts">Hesaplar</Link>
        </li>
        <li className={selectedTab === 'Siparişler' ? 'active' : ''} onClick={() => changeTab('Siparişler')}>
          <Link to="/orders">Siparişler</Link>
        </li>
      </ul>
    </div>
  );
}

export default Sidebar;
