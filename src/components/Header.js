// Header.js
import React from 'react';
import './Header.css'; // Header bileşeni için CSS dosyası

function Header() {
  const userName = "John Doe"; // Giriş yapanın adı ve soyadı

  return (
    <header className="header">
      <div className="profile">
        <img src="profil_fotografi_yolu" alt="Profil Fotoğrafı" />
        <div className="user-info">
          <p>{userName}</p>
          <p>Çıkış Yap</p>
        </div>
      </div>
      <div className="menu-toggle">
        <i className="fa fa-bars"></i>
      </div>
    </header>
  );
}

export default Header;
