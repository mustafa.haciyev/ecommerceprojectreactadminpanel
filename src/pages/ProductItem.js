// ProductItem.js

import React from 'react';

function ProductItem({ product }) {
    return (
        <li>
            <div>
                <h3>{product.name}</h3>
                <p>{product.description}</p>
                <p>Price: ${product.price}</p>
                <img src={product.image} alt={product.name} />
            </div>
        </li>
    );
}

export default ProductItem;
