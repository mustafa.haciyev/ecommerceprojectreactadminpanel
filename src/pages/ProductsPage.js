import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './ProductPage.css'; // Stil dosyasını ekleyin

const ProductPage = () => {
    // State'ler
    const [productName, setProductName] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [productImage, setProductImage] = useState('');
    const [productPrice, setProductPrice] = useState('');
    const [categoryName, setCategoryName] = useState('');
    const [categories, setCategories] = useState([]);
    const [products, setProducts] = useState([]);

    // Kategorileri ve ürünleri yükleme
    useEffect(() => {
        loadCategories();
        loadProducts();
    }, []);

    const loadCategories = async () => {
        try {
            const response = await axios.get('http://localhost:8085/api/category');
            setCategories(response.data);
        } catch (error) {
            console.error('Error loading categories:', error);
        }
    };

    const loadProducts = async () => {
        try {
            const response = await axios.get('http://localhost:8085/products/all');
            setProducts(response.data);
        } catch (error) {
            console.error('Error loading products:', error);
        }
    };

    // Ürün ekleme
    const addProduct = async () => {
        try {
            const productData = {
                name: productName,
                description: productDescription,
                image: productImage,
                price: productPrice,
                categoryName: categoryName
            };
            const response = await axios.post('http://localhost:8085/products/add', productData);
            console.log(response.data);
            // Başarılı eklemeyi işle
        } catch (error) {
            console.error('Error adding product:', error);
            // Hata durumunu işle
        }
    };

    // Ürün güncelleme
    const updateProduct = async (id) => {
        try {
            const updatedProductData = {
                name: productName,
                description: productDescription,
                image: productImage,
                price: productPrice,
                categoryName: categoryName
            };
            const response = await axios.put(`http://localhost:8085/products/edit/${id}`, updatedProductData);
            console.log(response.data);
            // Başarılı güncellemeyi işle
        } catch (error) {
            console.error('Error updating product:', error);
            // Hata durumunu işle
        }
    };

    // Ürün silme
    const deleteProduct = async (id) => {
        try {
            const response = await axios.delete(`http://localhost:8085/products/delete/${id}`);
            console.log(response.data);
            // Başarılı silmeyi işle
        } catch (error) {
            console.error('Error deleting product:', error);
            // Hata durumunu işle
        }
    };

    return (
        <div className="container"> {/* container sınıfını ekleyin */}
            <h2>Add Product</h2>
            <div className="form-group"> {/* form-group sınıfını ekleyin */}
                <label>Name:</label>
                <input type="text" value={productName} onChange={(e) => setProductName(e.target.value)} />
            </div>
            <div className="form-group"> {/* form-group sınıfını ekleyin */}
                <label>Description:</label>
                <input type="text" value={productDescription} onChange={(e) => setProductDescription(e.target.value)} />
            </div>
            <div className="form-group"> {/* form-group sınıfını ekleyin */}
                <label>Image URL:</label>
                <input type="text" value={productImage} onChange={(e) => setProductImage(e.target.value)} />
            </div>
            <div className="form-group"> {/* form-group sınıfını ekleyin */}
                <label>Price:</label>
                <input type="text" value={productPrice} onChange={(e) => setProductPrice(e.target.value)} />
            </div>
            <div className="form-group"> {/* form-group sınıfını ekleyin */}
                <label>Category:</label>
                <select value={categoryName} onChange={(e) => setCategoryName(e.target.value)}>
                    <option value="">Select a category</option>
                    {categories.map(category => (
                        <option key={category.id} value={category.categoryName}>{category.categoryName}</option>
                    ))}
                </select>
            </div>
            <div className="button-group"> {/* button-group sınıfını ekleyin */}
                <button onClick={addProduct}>Add Product</button>
            </div>

            <h2>All Products</h2>
            <ul className="product-list"> {/* product-list sınıfını ekleyin */}
                {products.map(product => (
                    <li key={product.id}>
                        <div>
                            <strong>{product.name}</strong> - {product.description} - {product.price}
                        </div>
                        <div className="button-group"> {/* button-group sınıfını ekleyin */}
                            <button onClick={() => updateProduct(product.id)}>Update</button>
                            <button onClick={() => deleteProduct(product.id)}>Delete</button>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default ProductPage;
