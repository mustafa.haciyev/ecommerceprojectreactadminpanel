import React, { useState, useEffect } from 'react';

function AccountsPage() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // Component mounts, fetch users
    fetchUsers();
  }, []); // Empty dependency array means it will run only once after initial render

  const fetchUsers = async () => {
    try {
      const response = await fetch('http://localhost:8080/users'); // Assuming this endpoint is correct
      if (!response.ok) {
        throw new Error('Failed to fetch users');
      }
      const userData = await response.json();
      setUsers(userData); // Update the state with fetched users
    } catch (error) {
      console.error('Error fetching users:', error.message);
    }
  };

  return (
    <div>
      <h2>Users List</h2>
      <ul>
        {users.map((user) => (
          <li key={user.id}>
            <p>Email: {user.email}</p>
            {/* You might display other user information here */}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default AccountsPage;
