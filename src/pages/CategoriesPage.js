import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FiEdit, FiTrash2 } from 'react-icons/fi';
import './CategoriesPage.css';



function CategoriesPage() {
  const [categories, setCategories] = useState([]);
  const [categoryName, setCategoryName] = useState('');
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = async () => {
    try {
      const response = await axios.get('http://localhost:8085/api/category');
      setCategories(response.data);
    } catch (error) {
      console.error('Error fetching categories:', error);
    }
  };

 const handleAddCategory = async () => {
    try {
        const response = await axios.post('http://localhost:8085/api/category/add', { categoryName });
        if (response && response.data) {
            setCategoryName('');
            fetchCategories();
            setSelectedCategory(response.data);
            if (window.opener) {
                window.opener.postMessage({ selectedCategory: response.data }, '*');
            }
        } else {
            console.error('Response or response.data is undefined.');
        }
    } catch (error) {
        console.error('Kategori eklenirken hata oluştu:', error.response.data);
        // Kullanıcı dostu geri bildirim sağlamak için uygun işlemler yapılabilir.
    }
};


  const handleEditCategory = async () => {
    try {
      await axios.put(`http://localhost:8085/api/category/edit/${selectedCategory.id}`, selectedCategory);
      fetchCategories();
      setEditMode(false);
      setSelectedCategory(null);
    } catch (error) {
      console.error('Error editing category:', error);
    }
  };

  const handleDeleteCategory = async (categoryId) => {
    try {
      await axios.delete(`http://localhost:8085/api/category/delete/${categoryId}`);
      fetchCategories();
    } catch (error) {
      console.error('Error deleting category:', error);
    }
  };

  const handleEditClick = (category) => {
    setSelectedCategory(category);
    setEditMode(true);
  };

  const handleCategoryNameChange = (event) => {
    setSelectedCategory({ ...selectedCategory, categoryName: event.target.value });
  };

  return (
    <div className="categories-page">
      <div className="add-category">
        <input
          type="text"
          placeholder="Kategori Adı"
          value={categoryName}
          onChange={(e) => setCategoryName(e.target.value)}
        />
        <button onClick={handleAddCategory}>Kategori Ekle</button>
      </div>
      <div className="category-list">
        <h2>Kategoriler</h2>
        <ul>
          {categories.map((category) => (
            <li key={category.id}>
              {category.categoryName} - {' '}
              <FiEdit onClick={() => handleEditClick(category)} />{' '}
              <FiTrash2 onClick={() => handleDeleteCategory(category.id)} />
            </li>
          ))}
        </ul>
      </div>
      {editMode && selectedCategory && (
        <div className="edit-category">
          <input
            type="text"
            value={selectedCategory.categoryName}
            onChange={handleCategoryNameChange}
          />
          <button onClick={handleEditCategory}>Kaydet</button>
        </div>
      )}
    </div>
  );
}

export default CategoriesPage;
